#' @title StandardGenerics
#' 
#' @description
#' Standard generics with methods for the Nolas class.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @name StandardGenerics
NULL

#' @title validate
#' 
#' @description
#' \code{validate} Performs cross validation on a Nolas model.
#' 
#' @aliases validate
#' @name StandardGenerics
#' @export
setGeneric("validate", function(object, k = 10, verbose = TRUE)
{ standardGeneric("validate") } )

#' @title confusionMatrix
#' 
#' @description
#' \code{confusionMatrix} Retrieves the confusion matrix produced by
#' \code{validate}
#' 
#' @aliases confusionMatrix
#' @name StandardGenerics
#' @export
setGeneric("confusionMatrix", function(object, className = NULL)
{ standardGeneric("confusionMatrix") } )

#' @title enrichment
#' 
#' @description
#' \code{enrichment} Performs enrichment analysis on features captured by LVs.
#' 
#' @aliases enrichment
#' @name StandardGenerics
#' @export
setGeneric("enrichment", function(object, lv, views = NULL, loadings = "all",
                                  dbs = c("GO_Biological_Process_2018",
                                          "GO_Molecular_Function_2018",
                                          "GO_Cellular_Component_2018"))
{ standardGeneric("enrichment") } )

#' @title scores
#' 
#' @description
#' \code{scores} Returns the global scores of samples.
#' 
#' @aliases scores
#' @name StandardGenerics
#' @export
setGeneric("scores", function(object)
{ standardGeneric("scores") } )

#' @title projection
#' 
#' @description
#' \code{projection} Returns projections of the model.
#' 
#' @aliases projection
#' @name StandardGenerics
#' @export
setGeneric("projection", function(object)
{ standardGeneric("projection") } )

#' @title featureNames
#' 
#' @description
#' \code{featureNames} Returns names of features selected in each view and LV.
#' 
#' @aliases featureNames
#' @name StandardGenerics
#' @export
setGeneric("featureNames", function(object, loadings = "all", views = NULL)
{ standardGeneric("featureNames") } )

#' @title numFeatures
#' 
#' @description
#' \code{numFeatures} Returns the number of features elected in each view and
#' LV.
#' 
#' @aliases numFeatures
#' @name StandardGenerics
#' @export
setGeneric("numFeatures", function(object)
{ standardGeneric("numFeatures") } )

#' @title fdr
#' 
#' @description
#' \code{fdr} Returns the adjusted p-values of features in each view and LV.
#' 
#' @aliases fdr
#' @name StandardGenerics
#' @export
setGeneric("fdr", function(object)
{ standardGeneric("fdr") } )

#' @title plotVarExp
#' 
#' @description
#' \code{plotVarExp} Plots proportion of variance explained per view and
#' globally.
#' 
#' @aliases plotVarExp
#' @name StandardGenerics
#' @export
setGeneric("plotVarExp", function(object, filename = NULL)
{ standardGeneric("plotVarExp") } )

#' @title plotScores
#' 
#' @description
#' \code{plotScores} Scatterplot of sample global scores.
#' 
#' @aliases plotScores
#' @name StandardGenerics
#' @export
setGeneric("plotScores", function(object, x = 1, y = 2, color = NULL,
                                  shape = NULL, annot = NULL,
                                  filename = NULL, flip = FALSE)
{ standardGeneric("plotScores") } )

#' @title plotClustering
#' 
#' @description
#' \code{plotClustering} Sample heatmap based on global scores.
#' 
#' @aliases plotClustering
#' @name StandardGenerics
#' @export
setGeneric("plotClustering", function(object, response = NULL, annot = NULL,
                                      filename = NULL)
{ standardGeneric("plotClustering") } )

#' @title plotLoadings
#' 
#' @description
#' \code{plotLoadings} Heatmap of LV loadings.
#' 
#' @aliases plotLoadings
#' @name StandardGenerics
#' @export
setGeneric("plotLoadings", function(object, lvs = NULL, views = NULL,
                                    cols = NULL, top = NULL,
                                    fontSize = 10, filename = NULL)
{ standardGeneric("plotLoadings") } )

#' @title plotFeatures
#' 
#' @description
#' \code{plotFeatures} Heatmap of original features in LVs.
#' 
#' @aliases plotFeatures
#' @name StandardGenerics
#' @export
setGeneric("plotFeatures", function(object, lv = NULL, top = NULL,
                                    response = NULL, annot = NULL,
                                    center = FALSE, scale = FALSE,
                                    corMethod = "pearson",
                                    fontSize = 10, filename = NULL,
                                    height = 7, width = 7)
{ standardGeneric("plotFeatures") } )

#' @title plotValidation
#' 
#' @description
#' \code{plotValidation} ROC from model validation.
#' 
#' @aliases plotValidation
#' @name StandardGenerics
#' @export
setGeneric("plotValidation", function(object, className = NULL,
                                      filename = FALSE)
{ standardGeneric("plotValidation") } )
