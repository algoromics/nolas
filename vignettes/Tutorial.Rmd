---
title: "NOLAS Tutorial"
subtitle: "Version 1.0.0"
author: "Benjamin Ulfenborg, PhD"
date: "13th August 2021"
output:
  prettydoc::html_pretty:
    toc: TRUE
vignette: >
  %\VignetteIndexEntry{NOLAS Tutorial}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
bibliography: vignettes/ref.bib
link-citations: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.cap = "")
```

# Introduction

### Matrix factorization

Integrative analysis of multi-omics data holds the promise to deliver novel and
actionable insights into health and disease. One popular approach to integrative
analysis is multiblock factor analysis, where several omics matrices are
decomposed into latent factors or variables, usually subject to a common score
matrix. These methods are capable of capturing variance structures within and
across omics modalities/views, and pave the way for discovery of multiomics
biomarker panels. This tutorial presents a novel matrix factorization technique
called NOise-reduced LAtent Structure (NOLAS) analysis, which takes a slightly
different perspective on inference of latent variables (LVs) compared to
existing methods.

### NOise-reduced LAtent Structure analysis

What distinguishes NOLAS from existing methods is the introduction of an LV-wise
feature selection step. LVs are computed consecutively using Singular Value
Decomposition (SVD), and the first right singular vector is used as feature
projection weights. The SVD is carried out on all omics views jointly, such
that covariance both within and across omics views will be captured.
The resulting weights W represent the extent of influence that the original
features have on the scores T, computed as $T = WX$. An initial LV is then
derived using all weights for projection, but many weights will be close to
zero and these features are assumed to have little or no influence on the
variance of the LV. This is the point of departure in NOLAS: the features with
weights close to zero are considered to contribute noise to the model, and they
should be removed to prevent noise from leaking into the LV scores.

### Sparsity

Some existing methods like sparse Partial Least Squares (sPLS) also perform
feature selection by imposing an L1 regularization penalty on the projection
weight vector. This simplifies the model by penalizing large weights and forcing
small weights to zero, which can then be dropped from the model. The advantage
is that is gives a more parsimonious model that can be more easily biologically
interpreted. The drawbacks are that this penalizes all features and requires
the user to set a sparsity penalty. This is an arbitrary threshold that
determines the number of features selected, but it does not confer statistical
or biological significance to those that are. Setting this threshold tends to be
difficult in practice, and may require a computationally demanding
optimization step for each dataset. This problem is exacerbated if the penalty
is set for every LV of the model. Rather than resorting to regularization, NOLAS
implements a method that is both computationally efficient and more intuitive to
use.

### Permutation testing

NOLAS carries out feature selection with a permutation-based test to assess the
statistical significance of the loadings between features and the initial LV
scores. This is done on an LV-basis, allowing LVs to selected
different subsets of features. The assumption is that, for features with
significant loadings, these will be much larger than when computed on
permuted data. For features with
loadings no greater than expected by chance, loadings will be about the same for
the original and permuted data. Only features with statistically significant
loadings will be retained by NOLAS, and a second SVD is carried out to compute
new weights W for those features. Weights of all non-significant features are
set to 0, followed by computation of new scores $T = WX$. These are the final
scores for the LV. X is then deflated to subtract the variance explained by the
LV, followed by a new iteration where the next LV is derived. In the final model
only significant features will be able to influence each LV; hence the method
is referred to as NOise-reduced LAtent Structure analysis. An advantage with
this technique is that it opens the possibility to detect multi-omic biomarker
panels, where included features are statistically significant by a
user-specified false discovery rate (FDR). This is a much easier and more
sensible threshold to decide on compared to a sparsity penalty.

### Rationale

NOLAS has been designed to map the LVs to the main biological
processes, pathways or other entities with mechanistic roles behind disease.
If the biological interpretation of LVs overlaps, if biologically relevant
features are difficult to identify, or if we are limited in the number of LVs we
can analyze, it will be difficult to translate the LVs into biological insights.
The aim with NOLAS is to infer a set of LVs that capture variance in several
subsets of features, with potentially low overlaps, representing different
biologically effects that can be more easily distinguished and interpreted. This
will be very important when integrating very large datasets from multiple omics
modalities, with the aim of translating biological insights and biomarkers into
precision medicine.

# Installation

The `nolas` package is currently hosted on GitLab and can be installed with
`devtools`.

```{r, eval = FALSE}
if(!require(devtools, quietly = TRUE)) install.packages("devtools")
devtools::install_git(url = "https://gitlab.com/algoromics/nolas.git")
```

# Multi-omics data integration with NOLAS

### Unsupervised analysis of doxorubicin-treated stem cell-derived cardiomyocytes

The first example will illustrate unsupervized analysis with NOLAS on stem-cell
derived cardiomyocytes, with or without treatment with doxorubicin. Experimental
data is available in ArrayExpress with accession number E-MTAB-6045. For
simplicity, the data has been processed and a subset is available within the
`nolas` package. We start by loading and inspecting the data.

```{r, eval = FALSE}
library(nolas)
data(doxo)
print(str(doxo))
```

```{r, echo = FALSE}
suppressMessages(library(nolas))
data(doxo)
print(str(doxo))
```

The dataset contains a total of 27 samples, treated with either 0 or 450nM
doxorubicin, and measured on day 0, 1, 2, 7 and 14 since the treatment was
applied. Omics modalities or views available include mRNA, miRNA and protein
expression.

The analysis is performed by calling `nolas` and supplying a list of matrices.
Variables must be on columns, so we need to transpose the matrices.
The number of LVs to infer is set with `nlv`, but is optional. If not set, NOLAS
will continue to infer LVs as long as statistically significant features are
found. If no features are found for an LV, it is dropped and the model fitting
stops. Significance level is set with `fdr`. Testing is carried out by
performing 1000 iterations of data permutation and calculating the loadings of
features to the inferred LV. It is possible to parallelize testing with
multiple CPUs by setting `ncpu`.

```{r, echo = FALSE}
data(doxo_model)
```

```{r, eval = FALSE}
doxoModel <- nolas(X = doxo[1:3], nlv = 2)
```

The most basic way to inspect the model is to look at the clustering of samples
in latent space. This is based on the latent variables inferred by NOLAS and may
reveal correlations between the LVs and properties of the samples. To interpret
the plot, we annotate the samples with treatment dose and day of measurement.

```{r, dpi = 300}
plotScores(doxoModel, annot = data.frame(Day = doxo$Day, Dose = doxo$Dose),
           color = "Day", shape = "Dose")
```

Here we see the untreated samples on the left, moving slightly upwards over
time, indicating an effect from keeping the cardiomyocytes in culture. The
treated day 1 and 2 samples are located in the bottom right, revealing the
acute effect of doxorubicin. Following treatment, there is a recovery period
where the samples move to the upper right of the plot. Thus LV1 appears to have
captured variance mainly attributed to treatment, while LV2 has captured changes
going on over time. The percentages along the axes indicate the fraction of
variance explained in the data across all omics views. Together the LVs explain
70 % of the variance in the input data. Here is a breakdown of variance per
latent variable and view.

```{r, dpi = 300}
plotVarExp(doxoModel)
```

The figure shows that the model has explained about 75 % of the variance in
the mRNA view, and about 40 % in miRNA and protein. Interestingly, LV1
(treatment effect) is dominant in mRNA, while LV2 (temporal effect) explains a
larger fraction of variance in miRNA and protein. Next we'll look at clustering
of samples based on the selected features with highest loadings. Here we take
the top 10 features within each LV and omics view and scale the expression
values to highlight differences between the sample groups.

```{r, dpi = 300}
plotFeatures(doxoModel, annot = data.frame(Day = doxo$Day, Dose = doxo$Dose),
             response = c("Day", "Dose"), top = 20, fontSize = 10, scale = TRUE)
```

We can call `numFeatures` to figure out how many features that have been
selected by the model from each view by each LV.

```{r}
numFeatures(doxoModel)
```

To look at the most important features selected by each LV, we can inspect their
loadings. Here we see the top 10 features, ranked by absolute value. The sign
indicates whether there is a positive or negative correlation between the
feature and the LV. Positive correlation implies that the feature value
increases when the LV score increases, and vice versa.

```{r, eval = FALSE}
plotLoadings(doxoModel, cols = 3, top = 10, fontSize = 10)
```

```{r, echo = FALSE, dpi = 200}
plotLoadings(doxoModel, cols = 3, top = 10, fontSize = 6)
```

The model can be biologically interpreter by way to annotation enrichment
analysis performed on the selected features in an LV, provided that these are
HGNC symbols. This will identify functional annotation terms that are
over-represented among the selected features when compared to the whole genome.
Here we identify significant biological processes and KEGG pathways for the
mRNAs and proteins selected by LV1.

```{r, eval = FALSE}
doxoEnrich <- enrichment(doxoModel, lv = 1, views = c("mRNA", "Protein"),
                         dbs = c("GO_Biological_Process_2018", "KEGG_2019_Human"))
plotEnrichment(doxoEnrich, nterms = 10, cols = 2, fontSize = 10)
```

```{r, echo = FALSE, dpi = 200}
doxoEnrich <- enrichment(doxoModel, lv = 1, views = c("mRNA", "Protein"),
                         dbs = c("GO_Biological_Process_2018", "KEGG_2019_Human"))
plotEnrichment(doxoEnrich, nterms = 10, cols = 2, fontSize = 6)
```

The model we looked at above selected features based on the permutation test
FDR < 5 %. Hundreds of features have been selected by this model,
and we may want to set more stringent criteria to select just handful of
markers. As complement to FDR, NOLAS offers the `rvc` parameter for setting a
cutoff on the relative variance contribution (RVC) of features to the LV. This
is akin to setting a fold change cutoff, which defines the smallest effect size
in terms of mean displacement that is considered biologically relevant. The
`rvc` parameter ranges from 0 to 1, where 0 implies no filtering and 1 implies
that one feature will be selected.

To select features by `rvc`, NOLAS first computes the variance contribution
$v_{f}$ of each feature $f$ to the LV. These correspond to the variance of
features in the recovered or reconstructed matrix $X_{rec} = TP^{t}$

$$v_{f} = var(X_{rec,f})$$

where $f$ denotes the column of $X_{rec}$. The underlying assumption is that
feature importance is proportional to their variance contribution, and thus the
most important feature is the one contributing the most variance, $max(v)$.
In order to select features, NOLAS then computes the relative variance
contribution as

$$u_{f} = \frac{v_{f}}{max(v)}$$

Features will be selected when $u_{f} > r$, where $r$ represents the `rvc`
parameter. This parameter thus reflects the importance of feature $f$ relative
to the most important one.

Here we fit a new model and set `rvc = 0.95`. In order to not penalize features
with lower total variance, we also scale the data.

```{r, eval = FALSE}
doxoModel2 <- nolas(X = doxo[1:3], nlv = 2, rvc = 0.95, scale = TRUE)
```

Now we can inspect the scores of the new model and compare them to those of
the previous one. This time the interpretation of the LVs is reversed, so L1
has (mainly) captured the temporal effect and LV2 the effect from treatment.
To make the results easier to compare, we can flip the axes by setting
`flip = TRUE`.

```{r, dpi = 300}
plotScores(doxoModel2, annot = data.frame(Day = doxo$Day, Dose = doxo$Dose),
           color = "Day", shape = "Dose", flip = TRUE)
```

Though the distances between samples have changed, the overall clustering is
similar to before, where the late time points with treated samples end up in
the top-right of the plot. When we check the number of features in the model,
we find that only a handful have been selected.

```{r}
numFeatures(doxoModel2)
```

We can easily find out the names of those features with `featureNames`.

```{r}
featureNames(doxoModel2)
```

Note that the model with two LVs now only explains about 40 % of the total
variance, compared to 70 % without `rvc` filtering. This is expected, since
fewer features have been used to compute the LV scores. At the same time, it is
only the selected features that will affect the scores, which means that the
clustering obtained for `doxoModel2` is entirely attributed to the expression
values of those features. Even though only a handful of features are used to
compute the LV, it can still explain variance present in the whole dataset and
is not restricted to only explaining the variance in selected features.

### Supervized analysis of breast cancer subtypes

NOLAS also supports supervized classification of samples based on the latent
variable model. Supervized NOLAS is based on PLS in that the aim is to fit a
model where the covariance between the LVs and a response variable Y is
maximized. The implementation supports both binary and multi-group
classification problems, where the latter is cast as a set of binary
classifications (one group vs the rest), against which a single model is
optimized.

For this example we will make use of a breast cancer dataset from TCGA prepared
in the [mixOmics package](http://mixomics.org) [@rohart2017mixomics].

```{r, warning = FALSE, message = FALSE}
if(!require(BiocManager, quietly = TRUE)) install.packages("BiocManager")
if(!require(mixOmics, quietly = TRUE)) BiocManager::install("mixOmics")
library(mixOmics)
data(breast.TCGA)
sapply(breast.TCGA$data.train[1:3], ncol)
```

The breast cancer dataset contains a subset of the mRNA, miRNA and protein
expression data, and is divided into a training set with 150 samples and a test
set with 70 samples. Here we will train a NOLAS model using the three omics
views in the training set. The NOLAS response variable is set with parameter
`Y` and should be a data frame with a column equal to a factor vector.
The name of the column to use for the supervized analysis is specified with
`response`. This is the molecular subtype of the tumor, either basal
(triple negative), HER2-enriched or Luminal A. Once the model is fit, we can
inspect the number of features selected and the LV scores with `numFeatures` and
`plotScores`.

```{r, eval = FALSE}
nolasBRCA <- nolas(X = breast.TCGA$data.train[1:3],
                   Y = data.frame(Subtype = breast.TCGA$data.train$subtype,
                                  row.names = rownames(breast.TCGA$data.train$mrna)),
                   response = "Subtype", nlv = 2)
numFeatures(nolasBRCA)
plotScores(nolasBRCA, color = "Subtype")
```

```{r, echo = FALSE, dpi = 300}
data(brca_model)
numFeatures(nolasBRCA)
plotScores(nolasBRCA, color = "Subtype")
```

The figure shows that the NOLAS model has achieved a nice separation between the
different subtypes. To select an even smaller subset of features, we again
introduce the `rvc` parameter.

```{r, eval = FALSE, fig.}
nolasBRCA2 <- nolas(X = breast.TCGA$data.train[1:3],
                   Y = data.frame(Subtype = breast.TCGA$data.train$subtype,
                                  row.names = rownames(breast.TCGA$data.train$mrna)),
                   response = "Subtype", nlv = 2, rvc = 0.25)
numFeatures(nolasBRCA2)
plotScores(nolasBRCA2, color = "Subtype")
```

```{r, echo = FALSE, dpi = 300}
numFeatures(nolasBRCA2)
plotScores(nolasBRCA2, color = "Subtype")
```

We can see that, even though NOLAS only selected a handful of features, it is
nonetheless able to achieve a strong separation between the subtypes. Looking at
these scores does not tell us so much about the model's performance, however,
as we have trained the model on the very samples in the plot. To obtain a more
realistic estimate of performance, we should validate the model by cross
validation or on an independent test set. We start with 5-fold cross validation
by calling `validate` and setting `k` (number of folds) to 5. To ensure that
the result is reproducible, we also set the random seed.

```{r, eval = FALSE}
set.seed(123)
nolasBRCA2 <- validate(nolasBRCA2, k = 5)
```

Validation results are stored in the `validate` slot in the NOLAS model. This is
a list where each element is one class in the response variable (tumor subtype).
We can retrieve classification results with `confusionMatrix` and extract
performance metrics for a specific class.

```{r, eval = FALSE}
valRes <- confusionMatrix(nolasBRCA2)
valRes$Basal
```

```{r, echo = FALSE}
valRes <- confusionMatrix(nolasBRCA2)
caret:::print.confusionMatrix(valRes$Basal)
```

From this we can see that the model has achieved 97 % accuracy and only
classified four of the samples wrongly. We can also visualize the results for
all classes simultaneously in a ROC plot.

```{r, dpi = 300}
plotValidation(nolasBRCA2)
```

To validate the model on an separate test set, we call `predict` and supply the
test set as `testData`. In this case the breast cancer test data only contains
expression values for mRNA and miRNA, but NOLAS can be applied anyway. The
model features present in the test data will then be used to predict the LV
scores for the samples (a warning about this will be issued).

```{r}
nolasPred <- predict(nolasBRCA2, testData = breast.TCGA$data.test[1:2])
```

Finally, we can obtain the confusion matrix and ROC curve for the test set with
`assessPredictions` and `plotPredictions`.

```{r, dpi = 300}
assessRes <- assessPredictions(nolasPred, breast.TCGA$data.test$subtype)
assessRes$Basal
plotPredictions(nolasPred, breast.TCGA$data.test$subtype)
```

These results show that the NOLAS model performed well in the cross validation
as well as on the test set samples.

# References
