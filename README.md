NOLAS
=====

NOise-reduced LAtent Structure analysis (NOLAS) is a matrix
factorization technique developed for integrative analysis of
multi-omics data. Unlike previously published methods, NOLAS includes a
factor-wise feature selection step to select statistically significant
features from each omics view. This allows the factors to select
different subsets of features, which could be used to detect
complementary multi-omic biomarker panels.

A introduction to the method and exampels for getting started are
available [here](https://algoromics.gitlab.io/nolas).

Installation
============

NOLAS can be installed by running the following commands in R.

``` r
if(!require(devtools, quietly = TRUE)) install.packages("devtools")
devtools::install_git(url = "https://gitlab.com/algoromics/nolas.git")
```
